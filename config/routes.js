/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝
  'GET /':                   { action: 'view-homepage-or-redirect' },
  'GET /welcome/:unused?':   { action: 'dashboard/view-welcome' },
  'GET /signup':             { action: 'entrance/view-signup' },
  'GET /login':              { action: 'entrance/view-login' },
  'GET /password/forgot':    { action: 'entrance/view-forgot-password' },
  'GET /password/new':       { action: 'entrance/view-new-password' },
  'GET /account':            { action: 'account/view-account-overview' },
  'GET /account/password':   { action: 'account/view-edit-password' },
  'GET /account/profile':    { action: 'account/view-edit-profile' },
  'GET /contact': 'view-contact',

  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝
  // Note that, in this app, these API endpoints may be accessed using the `Cloud.*()` methods
  // from the Parasails library, or by using those method names as the `action` in <ajax-form>.
  '/logout':                              { action: 'account/logout' },
  'PUT   /api/v1/account/update-password':               { action: 'account/update-password' },
  'PUT   /api/v1/account/update-profile':                { action: 'account/update-profile' },
  'PUT   /api/v1/entrance/login':                        { action: 'entrance/login' },
  'POST  /api/v1/entrance/signup':                       { action: 'entrance/signup' },
  'POST  /api/v1/entrance/send-password-recovery-email': { action: 'entrance/send-password-recovery-email' },
  'POST  /api/v1/entrance/update-password-and-login':    { action: 'entrance/update-password-and-login' },
  'POST  /api/v1/deliver-contact-form-message':          { action: 'deliver-contact-form-message' },



  //MENU API TA SELECTION
  'POST /apis/mahasiswa': 'apis/StudentController.create',
  'GET /apis/mahasiswa': 'apis/StudentController.find',
  'GET /apis/mahasiswa/:nim': 'apis/StudentController.findOne',
  'PATCH /apis/mahasiswa/:nim': 'apis/StudentController.update',
  'DELETE /apis/mahasiswa/:nim': 'apis/StudentController.delete',
  
  
  'POST /mahasiswa': 'apis/MahasiswaController.create',
  'GET /mahasiswa': 'apis/MahasiswaController.find',
  'GET /mahasiswa/:id': 'apis/MahasiswaController.findOne',
  'PATCH /mahasiswa/:id': 'apis/MahasiswaController.update',
  'DELETE /mahasiswa/:id': 'apis/MahasiswaController.delete',

  'POST /apis/dosen': 'apis/LecturerController.create',
  'GET /apis/dosen': 'apis/LecturerController.find',
  'GET /apis/dosen/:nik': 'apis/LecturerController.findOne',
  'PATCH /apis/dosen/:nik': 'apis/LecturerController.update',
  'DELETE /apis/dosen/:nik': 'apis/LecturerController.delete',

  'POST /apis/group': 'apis/GroupController.create',
  'GET /apis/group': 'apis/GroupController.find',
  'GET /apis/group/:id': 'apis/GroupController.findOne',
  'PATCH /apis/group/:id': 'apis/GroupController.update',
  'DELETE /apis/group/:id': 'apis/GroupController.delete',

  'POST /apis/topic': 'apis/TopicController.create',
  'GET /apis/topic': 'apis/TopicController.find',
  'GET /apis/topic/:id': 'apis/TopicController.findOne',
  'PATCH /apis/topic/:id': 'apis/TopicController.update',
  'DELETE /apis/topic/:id': 'apis/TopicController.delete',


  'GET /edit/mahasiswa' : 'editor/edit-mahasiswa',
  'GET /detail/mahasiswa/:id' : 'editor/detail-mahasiswa',
  'POST /update/mahasiswa/:id': 'apis/StudentController.updatebyId',
  'DELETE /edit/mahasiswa/:id' : 'apis/StudentController.deletebyId',
  
};
