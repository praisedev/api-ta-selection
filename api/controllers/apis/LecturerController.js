module.exports = {

  async create(req, res){
    try {

      let params = req.allParams();
      if(!params.name){
        return res.badRequest({err: 'Nama wajib diisi'});
      }

      const results = await Lecturer.create({
        name: params.name,
        nim: params.nim,
        email: params.email
      });
      return res.ok(results);
    }
    catch (err){
      return res.serverError(err);
    }
  },

 

  async find(req, res){
    var excl = ['hashedPassword','newPasswordToken','newPasswordTokenExpiresAt'];
    try {
      const apis = await Lecturer.find().omit(excl);
      return res.ok(apis);
    } catch (err) {
      return res.serverError(err);
    }
  },

  async findOne(req, res){
    var excl = ['hashedPassword','newPasswordToken','newPasswordTokenExpiresAt'];
    try {
      const apis = await Lecturer.findOne({
        nik: req.params.nik
      }).omit(excl);
      return res.ok(apis);
    } catch (err) {
      return res.serverError(err);
    }
  },

  async update(req, res){
    try {
      let params = req.allParams();
      let attributes = {};
      if(params.nama){
        attributes.nama = params.nama;
      }
      if(params.universitas){
        attributes.universitas = params.universitas;
      }
      if(params.alamat){
        attributes.alamat = params.alamat;
      }

      const apis = await Lecturer.update({id: req.params.id}, attributes, {updateAt: Date.now()});
      return res.ok(apis);
    } catch (err) {
      return res.serverError(err);
    }
  },

  async delete(req, res){
    try {
      const apis = await Lecturer.destroy({
        id: req.params.id
      });
      return res.ok(apis);
    } catch (err) {
      return res.serverError(err);
    }
  }
};