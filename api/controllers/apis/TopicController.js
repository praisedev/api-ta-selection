module.exports = {

  async create(req, res){
    try {

      let params = req.allParams();
      if(!params.name){
        return res.badRequest({err: 'Nama wajib diisi'});
      }

      const results = await Topic.create({
        name: params.name,
        nim: params.nim,
        email: params.email
      });
      return res.ok(results);
    }
    catch (err){
      return res.serverError(err);
    }
  },

 

  async find(req, res){
    var excl = ['hashedPassword','newPasswordToken','newPasswordTokenExpiresAt'];
    try {
      const apis = await Topic.find();
      return res.ok(apis);
    } catch (err) {
      return res.serverError(err);
    }
  },

  async findOne(req, res){
    var excl = ['hashedPassword','newPasswordToken','newPasswordTokenExpiresAt'];
    try {
      const apis = await Topic.findOne({
        nik: req.params.nik
      });
      return res.ok(apis);
    } catch (err) {
      return res.serverError(err);
    }
  },

  async update(req, res){
    try {
      let params = req.allParams();
      let attributes = {};
      if(params.nama){
        attributes.nama = params.nama;
      }
      if(params.universitas){
        attributes.universitas = params.universitas;
      }
      if(params.alamat){
        attributes.alamat = params.alamat;
      }

      const apis = await Topic.update({id: req.params.id}, attributes, {updateAt: Date.now()});
      return res.ok(apis);
    } catch (err) {
      return res.serverError(err);
    }
  },

  async delete(req, res){
    try {
      const apis = await Topic.destroy({
        id: req.params.id
      });
      return res.ok(apis);
    } catch (err) {
      return res.serverError(err);
    }
  }
};