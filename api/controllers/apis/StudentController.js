module.exports = {

  //
  async create(req, res){
    try {

      let params = req.allParams();
      if(!params.name){
        return res.badRequest({err: 'Nama wajib diisi'});
      }

      const apis = await Student.create({
        name: params.name,
        nim: params.nim,
        email: params.email
      });
      return res.ok(apis);
    }
    catch (err){
      return res.serverError(err);
    }
  },

  async find(req, res){
    var excl = ['hashedPassword','newPasswordToken','newPasswordTokenExpiresAt'];
    try {
      const apis = await Student.find().omit(excl);
      return res.ok(apis);
    } catch (err) {
      return res.serverError(err);
    }
  },

  async findOne(req, res){
    var excl = ['hashedPassword','newPasswordToken','newPasswordTokenExpiresAt'];
    try {
      const apis = await Student.findOne({
        nim: req.params.nim
      }).omit(excl);
      return res.ok(apis);
    } catch (err) {
      return res.serverError(err);
    }
  },

  async update(req, res){
    try {
      let params = req.allParams();
      let attributes = {};
      if(params.nama){
        attributes.nama = params.nama;
      }
      if(params.universitas){
        attributes.universitas = params.universitas;
      }
      if(params.alamat){
        attributes.alamat = params.alamat;
      }

      const apis = await Student.update({id: req.params.id}, attributes, {updateAt: Date.now()});
      return res.ok(apis);
    } catch (err) {
      return res.serverError(err);
    }
  },

  async updatebyId(req, res){
    var params = _.extend(req.query || {}, req.params || {}, req.body || {});
    var id = params.id;
    var url = "/detail/mahasiswa/" + id;
    
    if (!id) return res.send("No id specified.",500);

    Student.update(id, params, function Studentupdated(err, updatedStudent) {
      if (err) {
        res.redirect(url);
      }
      if(!updatedStudent) {
        res.redirect(url);
      }
      res.redirect(url);
    });
  },

  async delete(req, res){
    try {
      const apis = await Student.destroy({
        nim: req.params.nim
      });
      return res.ok(apis);
    } catch (err) {
      return res.serverError(err);
    }
  },

  async deletebyId(req, res){
    try {
      const apis = await Student.destroy({
        id: req.params.id
      });
      return res.ok(apis);
    } catch (err) {
      return res.serverError(err);
    }
  }
};