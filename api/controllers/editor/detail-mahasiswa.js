module.exports = async function (req, res) {
    const allStudent = await Student.find({id: req.params.id})
    const allPeminatan = await Peminatan.find({sort: [{id: 'asc'}]})
    res.view('pages/editor/detail-mahasiswa',
    {allStudent,allPeminatan}
    )
}