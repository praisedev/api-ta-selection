module.exports = async function (req, res) {
    const allStudent = await Student.find({sort: [{nim: 'asc'}]})
    const allPeminatan = await Peminatan.find({sort: [{id: 'asc'}]})
    res.view('pages/editor/edit-mahasiswa',
    {allStudent,allPeminatan}
    )
}